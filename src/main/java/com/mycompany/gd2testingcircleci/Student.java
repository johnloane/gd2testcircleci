/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2testingcircleci;

/**
 *
 * @author administrator
 */
public class Student 
{
    private String name;
    private int totalScore;
    private int numberQuizzes;
    
    public Student()
    {
        this.name = "NoName";
        this.totalScore = 0;
        this.numberQuizzes = 0;
    }
    
    public Student(String name, int score, int numQuiz)
    {
        this.name = name;
        this.totalScore = score;
        this.numberQuizzes = numQuiz;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public int getTotalScore()
    {
        return this.totalScore;
    }
    
    public double getAverageScore()
    {
        if(numberQuizzes != 0)
        { 
            return totalScore/numberQuizzes;
        }
        else
        {
            return -1;
        }
    }
    
    public int getNumberQuizzes()
    {
        return this.numberQuizzes;
    }
    
    public void addQuiz(int intScore)
    {
        if(intScore > 0)
        {
            totalScore += intScore;
            numberQuizzes++;
        }
    }
    
    
}

