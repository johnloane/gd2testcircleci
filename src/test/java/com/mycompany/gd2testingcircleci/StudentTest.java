/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gd2testingcircleci;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author administrator
 */
public class StudentTest {
    
    public StudentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Student.
     */
    @org.junit.Test
    public void testGetName() 
    {
        System.out.println("getName");
        testGetName("John");
        testGetName("");
    }

    
    public void testGetName(String name)
    {
        Student instance = new Student(name, 0, 0);
        String expResult = name;
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTotalScore method, of class Student.
     */
    @org.junit.Test
    public void testGetTotalScore() 
    {
        System.out.println("getTotalScore");
        Student instance = new Student();
        int expResult = 0;
        int result = instance.getTotalScore();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getAverageScore method, of class Student.
     */
    @org.junit.Test
    public void testGetAverageScore() 
    {
        System.out.println("getAverageScore");
        Student instance = new Student();
        double result = instance.getAverageScore();
        double expResult = -1.0;
        assertEquals(expResult, result, 0.0);
        instance.addQuiz(100);
        instance.addQuiz(100);
        instance.addQuiz(100);
        expResult = 100.0;
        result = instance.getAverageScore();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of getNumberQuizzes method, of class Student.
     */
    @org.junit.Test
    public void testGetNumberQuizzes() 
    {
        System.out.println("getNumberQuizzes");
        Student instance = new Student();
        instance.addQuiz(20);
        instance.addQuiz(40);
        int expResult = 2;
        int result = instance.getNumberQuizzes();
        assertEquals(expResult, result);  
    }

    /**
     * Test of addQuiz method, of class Student.
     */
    @org.junit.Test
    public void testAddQuiz() 
    {
        System.out.println("addQuiz");
        int intScore = 100;
        Student instance = new Student();
        instance.addQuiz(intScore);
        int expTotal = 100;
        int result = instance.getTotalScore();
        assertEquals(expTotal, result, 0.0);
        int expNumQuizzes = 1;
        result = instance.getNumberQuizzes();
        assertEquals(expNumQuizzes, result, 0.0);
        instance.addQuiz(-100);
        expTotal = 100;
        result = instance.getTotalScore();
        assertEquals(expTotal, result, 0.0);
    }
    
}
